{-# LANGUAGE OverloadedStrings #-}

module Main where

import           CmdArgsParser
import           Data
import           Extractor
import           Logging
import           Utils

import           Control.Lens
import           Control.Monad              (forM_)
import           Data.Aeson                 (encode)
import           Data.Aeson.Encode.Pretty   (encodePretty)
import qualified Data.ByteString.Lazy.Char8 as CL
import qualified Data.Text.Lazy             as TL
import           System.IO                  (hPutStrLn, stderr)

main :: IO ()
main = do
  cmdArgs <- parseCmdArgs <&> convertHandleToName
  let log = makeLogger cmdArgs
  log $ tshow cmdArgs
  extracted <- extractData cmdArgs
  log "Raw results:"
  log $ extracted & posts .~ [] & tshow
  forM_ (extracted ^. posts) $ tshow >>> log
  let encF = bool encode encodePretty (cmdArgs ^. pretty)
  extracted & encF & CL.unpack & putStrLn
  where
    convertHandleToName = userName %~ handleToName
