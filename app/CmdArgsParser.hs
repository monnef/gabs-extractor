{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE UnicodeSyntax     #-}

module CmdArgsParser where

import           Data
import           Utils

import           Data.Text                     (Text)
import qualified Data.Text                     as T
import           Options.Applicative
import qualified Options.Applicative           as OA
import           Text.InterpolatedString.Perl6 (qq)
import           Text.PrettyPrint.ANSI.Leijen  as PP
import           Text.Read                     (readMaybe)

readInt :: String -> Maybe Int
readInt = readMaybe

readText :: String -> Maybe Text
readText = T.pack >>> Just

intMaybeReader :: ReadM (Maybe Int)
intMaybeReader = maybeReader (readInt >>> pure)

textMaybeReader :: ReadM (Maybe Text)
textMaybeReader = maybeReader (readText >>> pure)

socialNetworkReader :: ReadM SocialNetworkType
socialNetworkReader = maybeReader parseSocNet
  where
    parseSocNet "gab"     = Just Gab
    parseSocNet "twitter" = Just Twitter
    parseSocNet _         = Nothing

cmdArgsParserInfo :: ParserInfo CmdArgs
cmdArgsParserInfo =
  OA.info
    (cmdArgsParser <**> helper)
    (fullDesc <> progDesc "Retrives posts of given user from Gab or Twitter." <> footerDoc (Just footerText))
  where
    footerText = example <> linebreak <> linebreak <> note <> linebreak <> linebreak <> madeBy <> linebreak
    example =
      "Example:" <> linebreak <> dquote <> white "gabs-extractor -u menfon -l 1 -p" <> dquote <+>
      "will retrieve first post of a user named menfon"
    note = "Note: Don't forget to start Selenium before using this program."
    madeBy = "  Made by" <+> magenta "monnef" <> ". Licensed under GPLv3." <+> copyleft
    copyleft = "\127279" <+> "2019"

cmdArgsParser :: Parser CmdArgs
cmdArgsParser =
  CmdArgs OA.<$> --
  strOption (long "user-name" <> short 'u' <> metavar "NAME" <> nameHelp) <*>
  option auto (long "timeout" <> short 't' <> metavar "SECONDS" <> value 10 <> timeoutHelp) <*>
  option intMaybeReader (long "limit" <> short 'l' <> value Nothing <> metavar "POSTS" <> limitHelp) <*>
  switch (long "keep-over-limit" <> short 'k' <> keepOverLimitHelp) <*>
  switch (long "pretty" <> short 'p' <> prettyHelp) <*>
  switch (long "verbose" <> short 'v' <> verboseHelp) <*>
  option socialNetworkReader (long "network" <> short 'n' <> value Gab <> metavar "SOC_NET" <> socNetHelp) <*>
  switch (long "fast" <> short 'f' <> fastHelp) <*>
  option textMaybeReader (long "profile-dir" <> short 'd' <> value Nothing <> metavar "DIR" <> profileDirHelp)
  where
    nameHelp = help "User name on social network"
    timeoutHelp = help "Maximum time to wait for load of a part of a page"
    limitHelp = help "Maximum number of posts"
    verboseHelp = help "Enable verbose output"
    prettyHelp = help "Enable pretty formating of output"
    keepOverLimitHelp = help "Process all loaded posts, include over-limit ones"
    socNetHelp = helpDoc $ Just $ "Social network type." <> linebreak <> [qq|Options: "gab" (default), "twitter"|]
    fastHelp = help "Disable random delays (simulation of human behavior)"
    profileDirHelp = help "Path to browser profile directory (persistent profile)"

parseCmdArgs :: IO CmdArgs
parseCmdArgs = customExecParser (prefs showHelpOnEmpty) cmdArgsParserInfo
