# gabs-extractor

Status: **Beta** (fully working, but some bugs may still be present)

Given user name `gabs-extractor` will get all posts and reposts (until all are obtained or limit is reached) from social network [Gab](https://gab.com) or Twitter. Output format is JSON.

![](screenshot.png)

## Requirements

* [stack](http://haskellstack.org)
* Java (to run Selenium)
* Chrome (Chromium)
* Chrome webdriver

## Install to stack bin directory

```sh
stack install
```

## Run

1) Start (and keep running) Selenium:
    ```sh
    $ cd selenium
    $ ./start-selenium.sh
    ```
2) Run `gabs-extractor`:
  * If installed:
    ```sh
    $ gabs-extractor -u menfon -l 1 -p
    ```
  * Or directly via Stack:
    ```sh
    $ stack exec -- gabs-extractor -u menfon -l 1 -p
    ``` 

## Notes
* Media (e.g. images) are *not* retrieved.
* Experimental version of a script automating retrieval of posts and packing results: `script/get_posts.sh`

# License
**GPL3**
