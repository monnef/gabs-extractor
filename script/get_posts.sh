#!/bin/bash
set -e
set -x

# <CONFIG>

# maximum number of posts to retrieve
limit=1

# user name on Gab
userGab=menfon

# user name on Twitter
userTwitter=monnef

# password for created archive
archPass=CHANGE_ME

# output directory
# note: directory nesting is NOT supported
outDir=out

# cmd to run to extract posts
cmd=gabs-extractor

# </CONFIG>

d=`date +%y%m%d`
dir=$outDir/$d
gabFile=$dir/${userGab}_gab_${limit}.json
twitterFile=$dir/${userTwitter}_twitter_${limit}.json

$cmd -h > /dev/null || exit 1
7z > /dev/null || exit 1

mkdir -p $dir

$cmd -u $userTwitter -l $limit -f -d test_profile -p -n twitter | tee $twitterFile
$cmd -u $userGab -l $limit -f -d test_profile -p -n gab | tee $gabFile

cd $dir
archName=sn${d}.7z
7z a $archName -p${archPass} ./*
mv $archName ..
cd ../..

echo "Done. Result is in archive '$outDir/$archName'."
