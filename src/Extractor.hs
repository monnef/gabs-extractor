{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ExtendedDefaultRules  #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE RecordWildCards       #-}

module Extractor
  ( extractData
  , ExtractorContext
  ) where

import           Data
import           ExtractorGab
import           ExtractorTwitter
import           Logging
import           Utils
import           WDUtils

import           Control.Lens
import           Data.Maybe                    (fromMaybe)
import qualified Data.Text                     as T
import           Test.WebDriver
import           Text.InterpolatedString.Perl6 (qq)

chromeConfig :: CmdArgs -> WDConfig
chromeConfig args = useBrowser chrome {chromeOptions = opts} defaultConfig
  where
    opts = fromMaybe [] $ (args ^. browserProfileDir) <&> profileDirToChromeArg <&> T.unpack <&> (: [])
    profileDirToChromeArg :: Text -> Text
    profileDirToChromeArg x = [qq|--user-data-dir=$x|]

extract :: SocialNetworkType -> ExtractorContext -> WD ExtractorResult
extract Gab     = extractFromGab
extract Twitter = extractFromTwitter

runInBrowser :: CmdArgs -> WD ExtractorResult
runInBrowser cmdArgs = extract (cmdArgs ^. snType) (ExtractorContext {..})

extractData :: CmdArgs -> IO ExtractorResult
extractData args = runSession (chromeConfig args) (runInBrowser args)
