{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ExtendedDefaultRules  #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module ExtractorTwitter
  ( extractFromTwitter
  ) where

import           Data
import           ExtractorUtils
import           Utils
import           WDUtils

import           Control.Conditional           (ifM)
import           Control.Lens
import           Control.Monad                 (unless, when)
import           Control.Monad.Trans           (liftIO)
import           Data.Maybe                    (fromMaybe, isJust, isNothing)
import qualified Data.Text                     as T
import           Data.Time                     (LocalTime, TimeZone, addUTCTime,
                                                defaultTimeLocale, formatTime,
                                                getCurrentTimeZone,
                                                iso8601DateFormat,
                                                localTimeToUTC, parseTimeM,
                                                timeZoneMinutes, utcToZonedTime)
import           Data.Time.Clock.POSIX         (posixSecondsToUTCTime)
import           Data.Time.Compat              (secondsToNominalDiffTime)
import           Logging                       (makeLogger)
import           System.Random                 (randomIO)
import           Test.WebDriver
import           Test.WebDriver.Commands.Wait
import           Test.WebDriver.JSON           (ignoreReturn)
import           Text.InterpolatedString.Perl6 (qq)
import           Text.Read                     (readMaybe)

twitterDateToIso8601 :: TimeZone -> Text -> Maybe Text
twitterDateToIso8601 tz dMay = do
  (d :: Int) <- dMay & T.unpack & readMaybe
  let parsedTime = d & realToFrac & posixSecondsToUTCTime
  return $ formatDateTime parsedTime

elemToStatus :: Int -> CmdArgs -> Element -> WD Post
elemToStatus idx args el = do
  log [qq|elemToStatus #$idx Starting processing {tshow el}|]
  when (idx == 0) $ do
    sendHomeKey
    shortWait args
  moveToCenter el
  shortWait args
  waitToFindElemFrom (args ^. timeout & realToFrac) el authorSel
  allText <- getText el
  log $ tshow allText
  _author <- findElemFrom el authorSel >>= getText <&> handleToName
  _replies <- actionCount el repliesSel
  _reposts <- actionCount el repostsSel
  _likes <- actionCount el likesSel
  _text <- getTextFromElemFromOrEmpty el textSel
  _dateRaw <- findElemFrom el dateSel >>= fattr "data-time" <&> fromMaybe ""
  tz <- liftIO getCurrentTimeZone
  let _date = _dateRaw & twitterDateToIso8601 tz & fromMaybe ""
  let _isRepost = (args ^. userName) /= _author
  let _ratioLikesVsReplies = fromIntegral _likes / fromIntegral _replies
  log [qq|elemToStatus #$idx Done $el: $_author, <3 $_likes, () $_reposts, T $_text|]
  return Post {..}
  where
    actionCount el sel =
      findElemFrom el sel >>= fattr "data-tweet-stat-count" <&> fmap numTextToIntOrZero <&> fromMaybe 0
    authorSel = ByCSS ".username"
    repliesSel = ByCSS ".ProfileTweet-actionList .ProfileTweet-action--reply .ProfileTweet-actionCount"
    repostsSel = ByCSS ".ProfileTweet-actionList .ProfileTweet-action--retweet .ProfileTweet-actionCount"
    likesSel = ByCSS ".ProfileTweet-actionList .ProfileTweet-action--favorite .ProfileTweet-actionCount"
    textSel = ByClass "tweet-text"
    dateSel = ByCSS ".time a span.js-short-timestamp"
    repostsActionIndex = 1
    likesActionIndex = 3 -- one before is hidden
    log = ("elemToStatus: " <>) >>> makeLogger args >>> liftIO

getPostElems :: WD [Element]
getPostElems = findElems (ByCSS "#timeline ol li .tweet")

microTimeout = 0.25

getHasMoreItemsEl :: WD (Maybe Element)
getHasMoreItemsEl =
  findBody >>= \b -> waitToFindElemFrom microTimeout b (ByCSS "#timeline .stream-footer .timeline-end.has-more-items")

getAllPosts :: CmdArgs -> WD [Post]
getAllPosts args = load []
  where
    shouldLoadMore :: [Post] -> WD Bool
    shouldLoadMore xs = do
      let loadMoreBcLength = maybe True (\lim -> length xs < lim) limitVal
      endOfTimelineEl <- getHasMoreItemsEl
      let reachedEnd = isNothing endOfTimelineEl
      onPagePosts <- getPostElems <&> length
      let reachedEndAndAllPostsProcessed = reachedEnd && onPagePosts == length xs
      return $ loadMoreBcLength && not reachedEndAndAllPostsProcessed
    limitVal = args ^. limit
    load :: [Post] -> WD [Post]
    load curRes = ifM (shouldLoadMore curRes) (loadNewPosts curRes) (return curRes)
    limitEls elsRaw = maybe elsRaw (\lim -> bool elsRaw (take lim elsRaw) (not $ args ^. keepOverLimit)) limitVal
    dropProcessedEls curRes = drop (length curRes)
    getNewPosts curRes els = els & zip [(length curRes) ..] <&> (\(idx, el) -> elemToStatus idx args el) & sequence
    loadNewPosts curRes = do
      sendEndKey
      longWait args
      elsRaw <- getPostElems
      newPosts <- elsRaw & limitEls & dropProcessedEls curRes & getNewPosts curRes
      load $ curRes ++ newPosts

extractFromTwitter :: ExtractorContext -> WD ExtractorResult
extractFromTwitter ExtractorContext {..} = do
  openPage url
  _userName <- findElem userNameSel >>= getText <&> handleToName
  _userTitle <- findElem userTitleSel >>= getText
  _userBio <- getTextFromElemOrEmpty userBioSel
  _posts <- getAllPosts cmdArgs
  let _socialNetwork = cmdArgs ^. snType
  _obtainedAt <- formattedNow
  log $ "Found " <> (length _posts & tshow) <> " posts."
  closeSession
  return ExtractorResult {..}
  where
    url = "https://twitter.com/" <> (cmdArgs ^. userName & T.unpack)
    log = makeLogger cmdArgs >>> liftIO
    userTitleSel = ByClass "ProfileHeaderCard-name"
    userNameSel = ByClass "ProfileHeaderCard-screenname"
    userBioSel = ByClass "ProfileHeaderCard-bio"
