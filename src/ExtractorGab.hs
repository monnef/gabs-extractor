{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ExtendedDefaultRules  #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module ExtractorGab
  ( extractFromGab
  ) where

import           Data
import           ExtractorUtils
import           Utils
import           WDUtils

import           Control.Conditional           (ifM)
import           Control.Lens
import           Control.Monad                 (unless, when)
import           Control.Monad.Trans           (liftIO)
import           Data.Maybe                    (fromMaybe, isJust)
import qualified Data.Text                     as T
import           Data.Time                     (LocalTime, TimeZone,
                                                defaultTimeLocale,
                                                getCurrentTime,
                                                getCurrentTimeZone,
                                                localTimeToUTC, parseTimeM)
import           Logging                       (makeLogger)
import           System.Random                 (randomIO)
import           Test.WebDriver
import           Test.WebDriver.Commands.Wait
import           Text.InterpolatedString.Perl6 (qq)

gabDateToIso8601 :: TimeZone -> Text -> Maybe Text
gabDateToIso8601 tz dMay = do
  let d = dMay & T.unpack
  parsedTime <- parseTimeM True defaultTimeLocale "%b %d, %Y, %H:%M" d :: Maybe LocalTime
  return $ formatDateTime (localTimeToUTC tz parsedTime)

elemToStatus :: Int -> CmdArgs -> Element -> WD Post
elemToStatus idx args el = do
  log [qq|elemToStatus #$idx Starting processing {tshow el}|]
  when (idx == 0) $ do
    sendHomeKey
    shortWait args
  moveToCenter el
  shortWait args
  waitToFindElemFrom (args ^. timeout & realToFrac) el authorSel
  allText <- getText el
  log $ tshow allText
  _author <- findElemFrom el authorSel >>= getText <&> handleToName
  actionEls <- findElemsFrom el actionSel
  _replies <- actionCount actionEls repliesActionIndex
  _reposts <- actionCount actionEls repostsActionIndex
  _likes <- actionCount actionEls likesActionIndex
  _text <- getTextFromElemFromOrEmpty el textSel
  _dateRaw <- findElemFrom el dateSel >>= \e -> attr e "title" <&> fromMaybe ""
  tz <- liftIO getCurrentTimeZone
  let _date = _dateRaw & gabDateToIso8601 tz & fromMaybe ""
  let _isRepost = (args ^. userName & nameToHandle) /= _author
  let _ratioLikesVsReplies = fromIntegral _likes / fromIntegral _replies
  log [qq|elemToStatus #$idx Done $el: $_author, <3 $_likes, () $_reposts, T $_text|]
  return Post {..}
  where
    actionCount els idx = els !! idx & getText <&> numTextToIntOrZero
    authorSel = ByClass "display-name__account"
    actionSel = ByClass "status__action-bar__counter"
    textSel = ByClass "status__content"
    dateSel = ByCSS ".status__relative-time time"
    repliesActionIndex = 0
    repostsActionIndex = 1
    likesActionIndex = 3
    log = ("elemToStatus: " <>) >>> makeLogger args >>> liftIO

getPostElems :: WD [Element]
getPostElems = findElems (ByCSS ".item-list > article")

microTimeout = 0.25

getAllPosts :: CmdArgs -> WD [Post]
getAllPosts args = load []
  where
    shouldLoadMore :: [Post] -> WD Bool
    shouldLoadMore xs = do
      let loadMoreBcLength = maybe True (\lim -> length xs < lim) limitVal
      loadMoreEl <- findBody >>= \b -> waitToFindElemFrom microTimeout b (ByClass "load-more")
      let loadMoreBcOfLoadMoreButtonEl = isJust loadMoreEl
      return $ loadMoreBcLength && loadMoreBcOfLoadMoreButtonEl
    limitVal = args ^. limit
    load :: [Post] -> WD [Post]
    load curRes = ifM (shouldLoadMore curRes) (loadNewPosts curRes) (return curRes)
    limitEls elsRaw = maybe elsRaw (\lim -> bool elsRaw (take lim elsRaw) (not $ args ^. keepOverLimit)) limitVal
    dropProcessedEls curRes = drop (length curRes)
    getNewPosts curRes els = els & zip [(length curRes) ..] <&> (\(idx, el) -> elemToStatus idx args el) & sequence
    loadNewPosts curRes = do
      sendEndKey
      longWait args
      elsRaw <- getPostElems
      newPosts <- elsRaw & limitEls & dropProcessedEls curRes & getNewPosts curRes
      load $ curRes ++ newPosts

extractFromGab :: ExtractorContext -> WD ExtractorResult
extractFromGab ExtractorContext {..} = do
  openPage url
  waitForLoadingIndicatorToVanish
  _userName <- findElem userNameSel >>= getText <&> handleToName
  _userTitle <- findElem userTitleSel >>= getText
  _userBio <- getTextFromElemOrEmpty userBioSel
  _posts <- getAllPosts cmdArgs
  let _socialNetwork = cmdArgs ^. snType
  _obtainedAt <- formattedNow
  log $ "Found " <> (length _posts & tshow) <> " posts."
  closeSession
  return ExtractorResult {..}
  where
    timeoutVal = cmdArgs ^. timeout & realToFrac
    url = "https://gab.com/" <> (cmdArgs ^. userName & T.unpack)
    waitForLoadingIndicatorToVanish = waitWhile timeoutVal $ findElem (ByCSS "div.loading-indicator")
    log = makeLogger cmdArgs >>> liftIO
    userTitleSel = ByCSS ".profile-info-panel-content__name > h1 > span"
    userNameSel = ByCSS ".profile-info-panel-content__name > h1 > small"
    userBioSel = ByClass "profile-info-panel-content__bio"
