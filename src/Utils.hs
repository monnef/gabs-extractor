{-# LANGUAGE OverloadedStrings #-}

module Utils
  ( tshow
  , tPutStrLn
  , (>>>)
  , (&)
  , (<&>)
  , Text
  , sleep
  , numTextToIntOrZero
  , dropPrefix
  , handleToName
  , nameToHandle
  , bool
  ) where

import           Control.Arrow              ((>>>))
import qualified Control.Arrow              as CA
import           Control.Concurrent         (threadDelay)
import           Data.Bool                  (bool)
import qualified Data.ByteString.Lazy       as BL
import qualified Data.ByteString.Lazy.Char8 as CL
import           Data.Function              ((&))
import           Data.Functor               ((<&>))
import           Data.Maybe                 (fromMaybe)
import           Data.Text                  (Text)
import qualified Data.Text                  as T
import qualified Data.Text.Lazy             as TL
import qualified Data.Text.Lazy.Encoding    as TLE
import           Text.Read                  (readMaybe)

tshow :: Show a => a -> Text
tshow = show >>> T.pack

tPutStrLn :: Text -> IO ()
tPutStrLn = T.unpack >>> putStrLn

mapTriple :: (a -> d, b -> e, c -> f) -> (a, b, c) -> (d, e, f)
mapTriple ~(f, g, h) ~(a, b, c) = (f a, g b, h c)

sleep :: Double -> IO ()
sleep secs = threadDelay $ round $ secs * 1000 * 1000

numTextToIntOrZero :: Text -> Int
numTextToIntOrZero = T.unpack >>> readMaybe >>> fromMaybe 0

dropPrefix :: Text -> Text -> Text
dropPrefix prefix t = fromMaybe t (T.stripPrefix prefix t)

handleToName :: Text -> Text
handleToName = dropPrefix "@"

nameToHandle :: Text -> Text
nameToHandle = handleToName >>> ("@" <>)
