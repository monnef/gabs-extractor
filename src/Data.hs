{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}

module Data where

import           Utils
import           UtilsTH

import           Control.Lens   hiding ((&))
import           Data.Aeson.TH  hiding (Options)
import           Data.Text      (Text)
import           GHC.Generics   (Generic)
import           Test.WebDriver (WD)

data SocialNetworkType
  = Gab
  | Twitter
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''SocialNetworkType

deriveJSON derJsonOpts ''SocialNetworkType

data CmdArgs =
  CmdArgs
    { _userName          :: Text
    , _timeout           :: Int
    --, _maxAge :: Maybe Date
    , _limit             :: Maybe Int
    , _keepOverLimit     :: Bool
    , _pretty            :: Bool
    , _verbose           :: Bool
    , _snType            :: SocialNetworkType
    , _fast              :: Bool
    , _browserProfileDir :: Maybe Text
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CmdArgs

deriveJSON derJsonOpts ''CmdArgs

data Post =
  Post
    { _author              :: Text
    , _likes               :: Int
    , _reposts             :: Int
    , _replies             :: Int
    , _ratioLikesVsReplies :: Double
    , _text                :: Text
    , _isRepost            :: Bool
    , _dateRaw             :: Text
    , _date                :: Text
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''Post

deriveJSON derJsonOpts ''Post

data ExtractorResult =
  ExtractorResult
    { _posts         :: [Post]
    , _userName      :: Text
    , _userTitle     :: Text
    , _userBio       :: Text
    , _socialNetwork :: SocialNetworkType
    , _obtainedAt    :: Text
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''ExtractorResult

deriveJSON derJsonOpts ''ExtractorResult

data ExtractorContext =
  ExtractorContext
    { cmdArgs :: CmdArgs
    }
