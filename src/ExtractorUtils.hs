{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module ExtractorUtils where

import           Data
import           Utils

import           Control.Lens
import           Control.Monad       (unless)
import           Control.Monad.Trans (liftIO)
import qualified Data.Text           as T
import           Data.Time           (FormatTime, defaultTimeLocale, formatTime,
                                      iso8601DateFormat, getCurrentTime)
import           System.Random       (randomIO)
import           Test.WebDriver

ifSimHuman :: CmdArgs -> a -> a -> a
ifSimHuman args t f =
  if args ^. fast
    then t
    else f

whenSimHuman :: CmdArgs -> WD () -> WD ()
whenSimHuman args = unless (args ^. fast)

randomWait :: CmdArgs -> Double -> Double -> WD ()
randomWait args rMin rMax =
  whenSimHuman args $ do
    (r :: Double) <- liftIO randomIO
    let range = rMax - rMin
    let duration = rMin + range * r
    liftIO $ sleep duration

shortWait :: CmdArgs -> WD ()
shortWait args = randomWait args 0.5 2.0

longWait :: CmdArgs -> WD ()
longWait args = randomWait args 1.5 5.0

attrViaJS :: Text -> Element -> WD (Maybe Text)
attrViaJS a el =
  executeJS
    [JSArg el, JSArg a]
    "console.log('attrViaJS', arguments); var res = arguments[0].attributes[arguments[1]]; console.log('attrViaJS', arguments, res); return res;"

formatDateTime :: (FormatTime t) => t -> Text
formatDateTime = formatTime defaultTimeLocale format >>> T.pack
  where
    format = iso8601DateFormat $ Just "%H:%M%EZ"

formattedNow :: WD Text
formattedNow = liftIO getCurrentTime <&> formatDateTime
