{-# LANGUAGE OverloadedStrings #-}

module WDUtils where

import           Utils

import           Test.WebDriver
import           Test.WebDriver.Commands
import           Test.WebDriver.Commands.Wait
import           Test.WebDriver.Common.Keys   (end, home)

findBody :: WD Element
findBody = findElem (ByTag "body")

waitToFindElemFrom :: Double -> Element -> Selector -> WD (Maybe Element)
waitToFindElemFrom t root sel = waitUntil t (findElemFrom root sel <&> Just) `onTimeout` return Nothing

getTextFromElemFromOrEmpty :: Element -> Selector -> WD Text
getTextFromElemFromOrEmpty root sel = do
  maybeEl <- waitToFindElemFrom 0.1 root sel
  maybe (return "") getText maybeEl

getTextFromElemOrEmpty :: Selector -> WD Text
getTextFromElemOrEmpty sel = findBody >>= flip getTextFromElemFromOrEmpty sel

sendEndKey :: WD ()
sendEndKey = findBody >>= sendKeys end

sendHomeKey :: WD ()
sendHomeKey = findBody >>= sendKeys home

fattr :: Text -> Element -> WD (Maybe Text)
fattr = flip attr
